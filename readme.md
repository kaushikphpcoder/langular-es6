## Laravel 5.3 Angular Material Starter

[![Latest Stable Version](https://poser.pugx.org/jadjoubran/laravel5-angular-material-starter/v/stable)](https://packagist.org/packages/jadjoubran/laravel5-angular-material-starter)
[![Latest Unstable Version](https://poser.pugx.org/jadjoubran/laravel5-angular-material-starter/v/unstable)](https://packagist.org/packages/jadjoubran/laravel5-angular-material-starter)
[![Build Status](https://travis-ci.org/jadjoubran/laravel5-angular-material-starter.svg?branch=master)](https://travis-ci.org/jadjoubran/laravel5-angular-material-starter)
[![StyleCI](https://styleci.io/repos/34944760/shield?style=flat)](https://styleci.io/repos/34944760)
[![Code Climate](https://codeclimate.com/github/jadjoubran/laravel5-angular-material-starter/badges/gpa.svg)](https://codeclimate.com/github/jadjoubran/laravel5-angular-material-starter)
[![License](https://poser.pugx.org/jadjoubran/laravel5-angular-material-starter/license)](https://packagist.org/packages/jadjoubran/laravel5-angular-material-starter)
[![Join the chat at https://gitter.im/jadjoubran/laravel5-angular-material-starter](https://badges.gitter.im/jadjoubran/laravel5-angular-material-starter.svg)](https://gitter.im/jadjoubran/laravel5-angular-material-starter?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

![Laravel & Angular](http://i.imgur.com/CwQy3Il.png)


## Docs & Demo

An online <a href="http://www.laravel-angular.io/" target="_blank">demo</a> is available.



Running on 3.2? Here are the [3.2 Docs](https://laravel-angular.readme.io/v3.2)

## Laravel 5 & Angular 2

Join the discussion on
- [Laravel & Angular](https://github.com/jadjoubran/laravel-angular)
- [Angular & Laravel](https://github.com/jadjoubran/angular-laravel)

## Screencasts

Screencasts on [Youtube](https://www.youtube.com/c/LaravelAngularMaterialStarter).


## Do It Yourself (Outdated)

A nice article on <a href="http://www.sitepoint.com/flexible-and-easily-maintainable-laravel-angular-material-apps/" target="_blank">sitepoint</a> that explains the first few versions of this repository. Recommended read if you're not familiar with the underlying technologies.

##Github main link

- https://github.com/jadjoubran/laravel5-angular-material-starter

##Bootstrap Theme Used

- https://blackrockdigital.github.io/startbootstrap-modern-business/index.html 

##installation

composer create-project jadjoubran/laravel5-angular-material-starter --prefer-dist
cd laravel5-angular-material-starter
npm install -g gulp bower
npm install
bower install
#fix database credentials in .env
php artisan migrate
gulp

## Git Local Repo update 

git fetch origin && git reset --hard origin/master && git clean -f -d
