<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'github' => [
        'client_id' => 'your-github-app-id',
        'client_secret' => 'your-github-app-secret',
        'redirect' => 'http://your-callback-url',
    ],

    'facebook'=>[
        'app_id' => '1850986321816551',
        'app_secret' => '6321a77f64d3e0096437b86448a957e3',
        'redirect' => 'http://your-callback-url',
    ],

    'google'=>[
        'client_id' => '381736853538-a4d2rj4f0e2b3q75tnrbjiginm96vear.apps.googleusercontent.com',
        'client_secret' => '6mjU18XjCl1txRHPlDOsHCe_',
        'redirect' => 'http://your-callback-url', 
    ] 

    ];
