<?php

namespace App\Model\Post;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table = 'post';

    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = ['post_title', 'post_subject', 'post_content']; 
}
